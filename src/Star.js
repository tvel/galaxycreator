
/**
 * Star
 */

function Star(number){
	
	Entity.call(this);
	this.type = "";
	this.color = "";
	this.mass = 0;
	this.temperatur  = 0;
	this.luminosity = 0;
	this.radius = 0;
	this.characteristics = "";
	this.infoContainer = new createjs.Container();;
	this.sunSystemNumber = number;
	this.name =GALAXY_NAME+"-"+this.sunSystemNumber+"S-0P";
	this.fullScaleBitmap;
}

Star.prototype.init = function () {
	var bitmap = BITMAP_CONTAINER[2][RANDOM.next(0,STAR_IMAGES_COUNT-1)].clone();
	this.fullScaleBitmap = bitmap.clone();
	
	Entity.prototype.init.call(this,bitmap.clone());

	// Setting Star properties 
	this.type = this.getType();
	this.color = this.getColor(this.type);
	this.mass = this.getMass(this.type);
	this.temperatur  = this.getTemperatur(this.type);
	this.luminosity = this.getLuminosity(this.type);
	this.radius = this.getRadius(this.type);
	
	//Sizing the star
	if(this.radius<3 && this.radius>1){
		this.scale = 0.5;
	}
	else if(this.radius>3){
		this.scale = 0.5;
	}
	else{
		this.scale = 0.4;
	}
	
	this.characteristics = this.getCharacteristics(this.type);

	// Setting star bitmap
	this.bitmap.x = CENTER_X-(this.bitmap.getBounds().width/2)*this.scale;
    this.bitmap.y = CENTER_Y-(this.bitmap.getBounds().height)*this.scale+this.bitmap.getBounds().height/6;
    this.bitmap.scaleX=this.scale;
    this.bitmap.scaleY=this.scale;

    // Setting back button
    this.backButton = createText("Back");
    this.backButton.x = 200 ;
    this.backButton.y = 900;

    // Creating Info container
    this.initInfoContainer();
};

Star.prototype.drawStarScreen = function(){
	stage.removeAllChildren();

	// Draw background
    stage.addChild(game.galaxy.sunSystems[this.sunSystemNumber].backgroundBitmap);
    
    // Show Star full image
    this.fullScaleBitmap.scaleX = 1.5;
    this.fullScaleBitmap.scaleY = 1.5;
    this.fullScaleBitmap.x = CENTER_X-(this.fullScaleBitmap.getBounds().width)*this.fullScaleBitmap.scaleX;
    this.fullScaleBitmap.y = CENTER_Y-this.fullScaleBitmap.getBounds().height+this.fullScaleBitmap.getBounds().height/6;
    stage.addChild(this.fullScaleBitmap);

    // Draw Info container
    stage.addChild(this.infoContainer);

    // Attach event listener to the back button 
    this.backButton.addEventListener('click',function(){
        game.currentScreen = 1;
    });
    stage.addChild(this.backButton);
}

Star.prototype.dispose = function (stage) {
	Entity.prototype.dispose.call(this, stage);
	createjs.Tween.removeTweens(this.bitmap);
};

// Create Info container
Star.prototype.initInfoContainer = function () {
	
	// Add frame
	var frame = new createjs.Graphics();
	frame.beginFill('black').drawRect(0, 0, 420,680 );
	var frameShape = new createjs.Shape(frame);
	frameShape.x = 1210;
	frameShape.y = 150;
	this.infoContainer.addChild(frameShape);

	var frameImg = BITMAP_CONTAINER[5][2].clone();
	frameImg.x = 1180;
    frameImg.y = 130;
    this.infoContainer.addChild(frameImg);

    // Add Star Name
	var obj = createText(this.name);
	obj.x = 1275;
	obj.y = 180;
	this.infoContainer.addChild(obj);

	// Add Star type
	obj = createText("Type: "+this.type,20);
	obj.x = 1250;
	obj.y = 250;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);

	// Add Star color
	obj = createText("Color: "+this.color,20);
	obj.x = 1250;
	obj.y = 290;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);

	// Add Star mass
	obj = createText("Mass: "+this.mass+" ME",20);
	obj.x = 1250;
	obj.y = 330;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);
	
	// Add Star Radius
	obj = createText("Radius: "+this.radius,20);
	obj.x = 1250;
	obj.y = 370;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);
	
	// Add Star Temperature
	obj = createText("Temperature: "+this.temperatur+" K",20);
	obj.x = 1250;
	obj.y = 410;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);

	// Add Star luminosity
	obj = createText("Luminosity: "+this.luminosity,20);
	obj.x = 1250;
	obj.y = 450;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);

	// Add Star Characteristics
	obj = createText("Characteristics: "+this.characteristics,20);
	obj.x = 1250;
	obj.y = 490;
	obj.lineWidth = 350;
	this.infoContainer.addChild(obj);
};

Star.prototype.draw = function () {
	this.bitmap.addEventListener('click',function(){
		game.currentScreen=2;
	});
	stage.addChild(this.bitmap);
};

// Get Star Type
Star.prototype.getType = function(){
	var type = "";
	switch(RANDOM.next(1,7)){
		case 1:
			type = "O";
			break;
		case 2:
			type = "B";
			break;
		case 3:
			type = "A";
			break;
		case 4:
			type = "F";
			break;
		case 5:
			type = "G";
			break;
		case 6:
			type = "K";
			break;
		case 7:
			type = "M";
			break;
	}
	return type;
}

// Get Star Color (Depending on Star Type)
Star.prototype.getColor = function(type){
	var color = "";
	switch(type){
		case "O":
			color = "Blue";
			break;
		case "B":
			color = "Blue";
			break;
		case "A":
			color = "Blue";
			break;
		case "F":
			var rand = RANDOM.next(1,2);
			if(rand = "1")color="White";
			if(rand = "2")color="Blue";
			break;
		case "G":
			var rand = RANDOM.next(1,2);
			if(rand = "1")color="White";
			if(rand = "2")color="Yellow";
			break;
		case "K":
			var rand = RANDOM.next(1,2);
			if(rand = "1")color="Orange";
			if(rand = "2")color="Red";
			break;
		case "M":
			color = "Red";
			break;
	}
	return color;
}

//Get Star Mass (Depending on Star Type)
Star.prototype.getMass = function(type){
	var mass = "";
	switch(type){
		case "O":
			mass = RANDOM.next(2500,8000);
			mass /=2;
			break;
		case "B":
			mass = RANDOM.next(1200,2500);
			mass /=2;
			break;
		case "A":
			mass = RANDOM.next(250,1200);
			mass /=2;
			break;
		case "F":
			mass = RANDOM.next(125,220);
			mass /=2;
			break;
		case "G":
			mass = RANDOM.next(90,125);
			mass /=2;
			break;
		case "K":
			mass = RANDOM.next(50,110);
			mass /=2;
			break;
		case "M":
			mass = RANDOM.next(10,50);
			mass /=2;
			break;
	}
	return mass;
}

//Get Star Temperature in Kelvin (Depending on Star Type)
Star.prototype.getTemperatur = function(type){
	var temperatur = "";
	switch(type){
		case "O":
			temperatur = RANDOM.next(25000,40000);
			break;
		case "B":
			temperatur = RANDOM.next(11000,25000);
			break;
		case "A":
			temperatur = RANDOM.next(7500,11000);
			break;
		case "F":
			temperatur = RANDOM.next(6000,7500);
			break;
		case "G":
			temperatur = RANDOM.next(5000,6000);
			break;
		case "K":
			temperatur = RANDOM.next(3500,5000);
			break;
		case "M":
			temperatur = RANDOM.next(2000,3500);
			break;
	}
	return temperatur;
}

//(Get Star Radius (Depending on Star Type)
Star.prototype.getRadius = function(type){
	var radius = "";
	switch(type){
		case "O":
			radius = RANDOM.next(800,5000);
			radius /= 100;
			break;
		case "B":
			radius = RANDOM.next(350,850);
			radius /= 100;
			break;
		case "A":
			radius = RANDOM.next(125,425);
			radius /= 100;
			break;
		case "F":
			radius = RANDOM.next(90,190);
			radius /= 100;
			break;
		case "G":
			radius = RANDOM.next(70,154);
			radius /= 100;
			break;
		case "K":
			radius = RANDOM.next(60,120);
			radius /= 100;
			break;
		case "M":
			radius = RANDOM.next(5,70);
			radius /= 100;
			break;
	}
	return radius;
}

// Get Star Luminosity (Depending on Star Type)
Star.prototype.getLuminosity = function(type){
	var luminosity = 0;
	switch(type){
		case "O":
			luminosity = RANDOM.next(75000000,220000000);
			luminosity /= 100;
			break;
		case "B":
			luminosity = RANDOM.next(750000,4500000);
			luminosity /= 100;
			break;
		case "A":
			luminosity = RANDOM.next(4000,16000);
			luminosity /= 100;
			break;
		case "F":
			luminosity = RANDOM.next(300,1200);
			luminosity /= 100;
			break;
		case "G":
			luminosity = RANDOM.next(50,220);
			luminosity /= 100;
			break;
		case "K":
			luminosity = RANDOM.next(10,100);
			luminosity /= 100;
			break;
		case "M":
			luminosity = RANDOM.next(1,12);
			luminosity /= 100;
			break;
	}
	return luminosity;
}

// Get Star Characteristics (Depending on Star Type)
Star.prototype.getCharacteristics = function(type){
	var characteristics = "";
	switch(type){
		case "O":
			characteristics = "Singly ionized helium lines (H I) either in emission or absorption. Strong UV continuum.";
			break;
		case "B":
			characteristics = "Neutral helium lines (H II) in absorption.";
			break;
		case "A":
			characteristics =  "Hydrogen (H) lines strongest for A0 stars, decreasing for other A's.";
			break;
		case "F":
			characteristics = "Ca II absorption. Metallic lines become noticeable."; 
			break;
		case "G":
			characteristics = "Absorption lines of neutral metallic atoms and ions (e.g. once-ionized calcium)."; 
			break;
		case "K":
			characteristics = "Metallic lines, some blue continuum."; 
			break;
		case "M":
			characteristics = "Some molecular bands of titanium oxide."; 
			break;
	}
	return characteristics;
}

