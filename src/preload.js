var i = 0;


//Canvas dimensions
var canvas;
var width;
var height;

// stage dimensions
var stage;
var stageDimWidth = 1920;
var stageDimHeight = 1080;
var scale;

//Game containers
var game;
var pauseMenu;
var backImage = new createjs.Bitmap("img/StartScreen/start"+Math.floor((Math.random()*26)+1)+".jpg");

var manifest;
var preload;
    
//Grid settings
var gridSettingsContainer = new GridSettings;

var hitSpriteTemplate;

//Click containers
var mouseClicked = false;
var mouseClickedLength = 0;
var mouseClickedX;
var mouseClickedY;

var tick = 0;

var mainMenu = new createjs.Container();

//Grid settings and functions

function GridSettings() {
    this.horTilesLength = 64;
    this.verTilesLength = 64;
    this.gridXStartCoord = 0;//96;
    this.gridYStartCoord = 0;//48;
    this.verTilesCount = 12;//11;
    this.horTilesCount = 12;// 10;
}


//INIT canvas and stage
//enables touch 
//Sets scale
//Initialise Start Menu
function init(){

    canvas = document.getElementById("canvas");
    stage = new createjs.Stage("canvas");
    stage.enableMouseOver();
    createjs.Touch.enable(stage);

    setScale();
    
    var progressText = new createjs.Text("", "20px Arial", "#000000");
    progressText.x = 300 - progressText.getMeasuredWidth() / 2;
    progressText.y = 20;
    stage.addChild(progressText);
    stage.update();
     
    
    
    setupManifest();
    startPreload();

    //initMenu();
}

function setupManifest() {
    for (var i = 1; i < 26; i++) {
        manifest.push({src:"img/StartScreen/start"+i+".jpg","i"});   
    }
}
 
function startPreload() {
    preload = new createjs.LoadQueue(true);
    preload.on("fileload", handleFileLoad);
    preload.on("progress", handleFileProgress);
    preload.on("complete", loadComplete);
    preload.on("error", loadError);
    preload.loadManifest(manifest);
 
}
 
function handleFileLoad(event) {
    console.log("A file has loaded of type: " + event.item.type);
    if(event.item.id == "5"){
        console.log("5 is loaded");
        //create bitmap here
    }
}
 
 
function loadError(evt) {
    console.log("Error!",evt.text);
}
 
 
function handleFileProgress(event) {
    progressText.text = (preload.progress*100|0) + " % Loaded";
    stage.update();
}
 
function loadComplete(event) {
    console.log("Finished Loading Assets");
}
//Setting scale
function setScale(){
    width = parseInt($("canvas").width());
    height = parseInt($("canvas").height());

    // keep aspect ratio
    scale = Math.min(width / stageDimWidth, height / stageDimHeight);
    stage.scaleX = scale;
    stage.scaleY = scale;

    stage.canvas.width = stageDimWidth * scale;
    stage.canvas.height = stageDimHeight * scale;

}

//Initialise Menu. 
//Adding event Listener to "Start" click
//The input field should be filled for a game to start
function initMenu() {
    var startText = createText("Start");
    startText.x = stageDimWidth / 2 - 50;
    startText.y = stageDimHeight / 2;
    
    startText.addEventListener('click', function() {
        if($("#input").val()!=""){
            stage.removeChild(mainMenu);
            $("#input").hide();     
            initGame();
        }
        
    });

    mainMenu.addChild(startText);
    
    var domElement = new createjs.DOMElement("input");
     domElement.htmlElement.onclick = function() {
    }
    domElement.x= stageDimWidth / 2 - 70;
    domElement.y= stageDimHeight / 2;
    //$("#input").height(30*scale);
    //$("#input").width(150*scale);
    backImage = new createjs.Bitmap("img/StartScreen/start"+Math.floor((Math.random()*26)+1)+".jpg");
    stage.addChild(domElement);
    console.log(backImage);
    stage.addChild(backImage);
    stage.addChild(mainMenu);


    var progressText = new createjs.Text("", "20px Arial", "#000000");
progressText.x = 300 - progressText.getMeasuredWidth() / 2;
progressText.y = 20;
stage.addChild(progressText);
//stage.update();

    stage.update();
}

//Function is called after a number is entered
//Starts by building the Nebula/Stars/Planets (In this order)

function initGame() {
    game = new Game(width, height, stage, gridSettingsContainer,$("#input").val());
    game.init(1);

    draw();
    this.document.onkeyup = keyBoardHandler;
    this.document.onmousedown = mouseDownHandler;
    this.document.onmouseup = mouseUpHandler;
    createjs.Ticker.addEventListener("tick", tick);

}

var creation = new createjs.Container();
function draw(){

    console.log(game);
    stage.addChild(game.galaxy.bitmap);
    var creationText = createText("Sun Systems: "+game.galaxy.sunSystems.length);
    creationText.x = 0 ;
    creationText.y = 0 ;
    creation.addChild(creationText);
    for (var i = 0; i < game.galaxy.sunSystems.length; i++) {
        
        creationText = createText("Sun: "+game.galaxy.sunSystems[i].stars.length);
        creationText.x = 0 ;
        creationText.y =  (i+1)*30;
        creation.addChild(creationText);
        creationText = createText("Planets: "+game.galaxy.sunSystems[i].planets.length);
        creationText.x = 150 ;
        creationText.y = (i+1)*30 ;

        creation.addChild(creationText);    
    };
    stage.addChild(creation);
    stage.update();
    //game.draw();
        

}

function mouseDownHandler(event) {
    if (!game.paused) {
        mouseClicked=true;
        mouseClickedX = event.pageX;
        mouseClickedY = event.pageY;
        console.log(event.pageX);
        console.log(event.pageY);
    }
}
function mouseUpHandler() {
    if (!game.paused) {
        mouseClicked=false;
        mouseClickedLength=0;
        mouseClickedX=0;
        mouseClickedY=0;
    }
}

function keyBoardHandler(event) {
    
    // when 'p' is pressed (for pause)
    if (event.keyCode == 80) {
        if (!game.paused) {
            game.pause();
            stage.addChild(getPauseMenu());
            stage.update(); 
        }
        else {
            stage.removeChild(getPauseMenu());
            game.resume()
        }
    } else if (event.keyCode == 27) { // on escape
        game.dispose();
        stage.removeAllChildren();
        initMenu();
    }
}

function getPauseMenu() {
    if (pauseMenu == null) {
        pauseMenu = new createjs.Container();
        var popupGraphics = new createjs.Graphics();
        popupGraphics.beginFill('black').drawRect(stageDimWidth / 2 - 150, stageDimHeight / 2 - 150, 300, 300);
        var popupShape = new createjs.Shape(popupGraphics);
        pauseMenu.addChild(popupShape);
        
        var pauseText = createText("Game paused");
        pauseText.x = stageDimWidth / 2 - 130;
        pauseText.y = stageDimHeight / 2 - 30;
        pauseMenu.addChild(pauseText);
    }
    
    return pauseMenu;
}

function mouseClicking() {
    console.log(mouseClickedX);
    /*if (mouseClicked)mouseClickedLength += 1;
    if (mouseClickedLength > 1) {
        var tile = Entity.prototype.getTile(game.grid, mouseClickedX, mouseClickedY);
        if (undefined != tile) {
            game.addTower(tile.x, tile.y);
            stage.update();
        }
    }*/
}

function tick() {
    if (!game.paused) {
        mouseClicking();
        tick++;
        console.log(tick);
        /*if (i++ % 10 == 0) {
            game.addEnemy();
            //Gold giver Dev cheat
            game.gold+=100;
        }
        if (i % 20 == 0){
            //console.log(game.towers);

            for (var j = 0; j < game.towers.length; j++) {
                // console.log("Updating tower " + j);
                game.towers[j].update(game);
            }
        }*/
        //Refresh lives/gold
        //game.addLivesText();
        //game.addGoldText();
       // stage.addChild(creation);
        stage.update(); 
    }    
}

function createText(text) {
    var textObj = new createjs.Text(text, "40px Arial", "#ff7700");
    var hit = new createjs.Shape();
    hit.graphics.beginFill("#000").drawRect(0, 0, textObj.getMeasuredWidth(), textObj.getMeasuredHeight());
    textObj.hitArea = hit;
    
    return textObj;
}



