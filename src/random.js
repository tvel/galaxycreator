//Main random generator class
//Function next returns the next random number
//which is created by the seed
function SeededRandom(seed){
    this.seed = seed;
    this.count = 1;
    Math.seedrandom(seed);

    SeededRandom.prototype.next = function(min,max){
        this.count++;
        //return Math.floor((Math.random()*max)+min) ;
         return Math.floor(Math.random() * (max - min + 1) + min);
    }

    SeededRandom.prototype.getCount = function(){
        return this.count;
    }

    SeededRandom.prototype.nextProz = function(proz){
        this.count++;
        var rand = Math.floor((Math.random()*100)+1*proz);
        if(rand == 100) return true;
        else return false;
    }
}
