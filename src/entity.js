function Entity() {
    //this.game = game;
    //this.random = random;
    this.bitmap;

}

Entity.prototype.init = function (bitmap) {
    this.bitmap = bitmap;
    this.bitmap.x =0;
    this.bitmap.y = 0;
};

Entity.prototype.dispose = function (stage) {
    stage.removeChild(this.bitmap);
};
