// Canvas dimensions
var canvas;
var width;
var height;

// stage dimensions
var stage;
var stageDimWidth = 1920;
var stageDimHeight = 1080;
var scale;

// Game containers
var game;
var pauseMenu;
var backImage;

var progressText="";
var manifest=[];
var preload;

// Counts
var START_IMAGES_COUNT = 26;
var GALAXY_IMAGES_COUNT = 33;
var STAR_IMAGES_COUNT = 30;
var PLANET_IMAGES_COUNT = 240;
var SYSTEM_IMAGES_COUNT = 31;
var ASSETS_IMAGES_COUNT = 2;

// Bitmaps
var startBitmap = [];
var galaxyBitmap = [];
var starBitmap = [];
var planetBitmap = [];
var systemBitmap = [];
var assetsBitmap = [];

// Grid settings
var BITMAP_CONTAINER = [startBitmap,galaxyBitmap,starBitmap,planetBitmap,systemBitmap,assetsBitmap];
var RANDOM;
var hitSpriteTemplate;

// Galaxy Name
var GALAXY_NAME;

// Click containers
var mouseClicked = false;
var mouseClickedLength = 0;
var mouseClickedX;
var mouseClickedY;

// Initialise tween container
var tween = [];
var testPlanet;

var mainMenu = new createjs.Container();
var progressText;

var recalcCounter=0;

var CENTER_X = stageDimWidth/2;
var CENTER_Y = 500;

// Grid settings
function GridSettings() {
    this.horTilesLength = 64;
    this.verTilesLength = 64;
    this.gridXStartCoord = 0;
    this.gridYStartCoord = 0;
    this.verTilesCount = 12;
    this.horTilesCount = 12;
}


function init() {
    //INIT canvas and stage
	canvas = document.getElementById("canvas");
	stage = new createjs.Stage("canvas");
    
    //enables touch 
    stage.enableMouseOver();
    createjs.Touch.enable(stage);

	//Sets scale
    setScale();
	
    //Initialise Start Menu
	var progressText = new createjs.Bitmap("img/loading.jpg");
    stage.addChild(progressText);
    stage.update();
     
    var loadingText = new createjs.Text("Loading", "40px Arial", "#000000");
    loadingText.x = stageDimWidth / 2 -20 ;
    loadingText.y = stageDimHeight / 2 - 150;
    stage.addChild(loadingText);
    stage.update();

    //Sets manifest and preloads resources
    setupManifest();
    startPreload();
}

//Setting scale
function setScale(){
	width = parseInt($("canvas").width());
    height = parseInt($("canvas").height());

	// keep aspect ratio
	scale = Math.min(width / stageDimWidth, height / stageDimHeight);
	stage.scaleX = scale;
	stage.scaleY = scale;

	stage.canvas.width = stageDimWidth * scale;
	stage.canvas.height = stageDimHeight * scale;

}

//Initialize Menu. 
//Adding event Listener to "Start" click
//The input field should be filled for a game to start
function initMenu() {
	// Show input field
    $("input").show();
	
    // Create Start Button 
    var startText = createText("Start");
	startText.x = stageDimWidth / 2 - 50;
	startText.y = stageDimHeight / 2;
	
    //Add event listener for Start Button
	startText.addEventListener('click', function() {
		if($("#input").val()!=""){

            //remove main manu
			stage.removeChild(mainMenu);
			$("#input").hide();		
			
            //Start Game
            initGame();
		}
		
	});

	mainMenu.addChild(startText);
	
    // Start button
	var domElement = new createjs.DOMElement("input");
	 domElement.htmlElement.onclick = function() {
 	}

 	domElement.x= stageDimWidth / 2 - 70;
 	domElement.y= stageDimHeight / 2-150;
 	stage.addChild(domElement);
 	
    // Set background image (Start Menu)
    backImage = BITMAP_CONTAINER[0][Math.floor((Math.random()*START_IMAGES_COUNT)+0)];
	stage.addChild(backImage);
	stage.addChild(mainMenu);
	stage.update();
}

function initGame() {

	// Initialize random generatior with supplied seed
    RANDOM = new SeededRandom($("#input").val());
    
    // Setting Galaxy name
    setGalaxyName();
	
    // Initialize game
    game = new Game(width, height, stage);
    game.init();

    // Adding tick event
	createjs.Ticker.addEventListener("tick", tick);
}

// Tick Listener
function tick() {

	if (!game.paused) {
        
        game.draw();
	}    
}

// Helper for text creation 
function createText(text,size) {
    var fontSize = 40;

    if(size>=0)fontSize=size;

    var textObj = new createjs.Text(text, fontSize+"px freedom", "#ff7700");
	var hit = new createjs.Shape();
	hit.graphics.beginFill("#000").drawRect(0, 0, textObj.getMeasuredWidth(), textObj.getMeasuredHeight());
	textObj.hitArea = hit;
	
	return textObj;
}

// Setup preload manifest
function setupManifest(){
    for(var i = 1; i <= START_IMAGES_COUNT; i++){
        manifest.push({src:"img/start/start"+i+".jpg",start_id:i});   
    }
    for(var i = 1; i <= GALAXY_IMAGES_COUNT; i++){
        manifest.push({src:"img/galaxy/galaxy"+i+".jpg",galaxy_id:i});   
    }
    for(var i = 1; i <= STAR_IMAGES_COUNT; i++){
        manifest.push({src:"img/stars/star"+i+".png",star_id:i});   
    }
    for(var i = 1; i <= PLANET_IMAGES_COUNT; i++){
        manifest.push({src:"img/planets/planet"+i+".png",planet_id:i});   
    }
    for(var i = 1; i <= SYSTEM_IMAGES_COUNT; i++){
        manifest.push({src:"img/system/system"+i+".jpg",system_id:i});   
    }
    manifest.push({src:"img/assets/tile.png",assets_id:1});
    manifest.push({src:"img/assets/frame.png",assets_id:2});   
    manifest.push({src:"img/assets/frame2.png",assets_id:3}); 

}

// Start preloading resources
function startPreload() {
    preload = new createjs.LoadQueue(false);
    preload.on("fileload", handleFileLoad);
    preload.on("progress", handleFileProgress);
    preload.on("complete", loadComplete);
    preload.on("error", loadError);
    preload.loadManifest(manifest);
 
}

// Setting file in the right container
function handleFileLoad(event) {

    if(event.item.start_id){
        startBitmap.push(new createjs.Bitmap(event.item.src));
    }
    else if(event.item.galaxy_id){
        galaxyBitmap.push(new createjs.Bitmap(event.item.src));
    }
    else if(event.item.star_id){
        starBitmap.push(new createjs.Bitmap(event.item.src));
    }
    else if(event.item.planet_id){
        planetBitmap.push(new createjs.Bitmap(event.item.src));
    }
    else if(event.item.system_id){
        systemBitmap.push(new createjs.Bitmap(event.item.src));
    }
    else if(event.item.assets_id){
        assetsBitmap.push(new createjs.Bitmap(event.item.src));
    }
}
 
// Error while loading resources
function loadError(evt) {
}
 
// Loading text visualization
function handleFileProgress(event) {
    stage.removeChild(progressText);
    stage.update();
    progressText = new createjs.Text((preload.progress*100|0) + " %", "40px Arial", "#000000");
    progressText.x = stageDimWidth / 2 ;
    progressText.y = stageDimHeight / 2 - 50;
    stage.addChild(progressText);
    stage.update();
}
 
// Init Main Menu
function loadComplete(event) {
    initMenu();
}